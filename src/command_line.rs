use std::collections::HashMap;
use regex::Regex;


// Low level Command-line argument processor
//
// Consume an iterator of command-line arguments and
// separate them into a Vector of positional arguments
// and a HashMap of keyword arguments or flags.
pub fn read_args<T>(mut args: T, flags: &[&str])
    -> Result<(Vec<String>, HashMap<String, String>), String>
where
    T: Iterator<Item = String>,
{
    let mut positional = Vec::new();
    let mut keyword = HashMap::new();

    if let Some(program_name) = args.next() {
        positional.push(program_name);
    }

    // Everything that starts with "-" and is not followed by a number
    // is expected to be followed with a value, except if it is in `flags`.
    let beg_re = Regex::new(r"^-[\di]").unwrap();
    loop {
        if let Some(arg) = args.next() {

            if arg.starts_with("-") && !beg_re.is_match(&arg){
                if flags.contains(&&*arg) {
                    keyword.insert(arg, String::new());
                } else {
                    if let Some(next_arg) = args.next() {
                        keyword.insert(arg, next_arg);
                    } else {
                        return Err(format!("Expecting an argument after {}",
                                           arg));
                    }
                }
            } else {
               positional.push(arg.to_lowercase());
            }
        } else {
            break;
        }
    }
    Ok((positional, keyword))
}


#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_read_args() {
        let mut expected_pos = Vec::new();
        expected_pos.push("prog");
        expected_pos.push("aa");
        expected_pos.push("dd");
        let mut expected_kw = HashMap::new();
        expected_kw.insert("--bb".to_string(), "cc".to_string());
        expected_kw.insert("--ee".to_string(), String::new());

        if let Ok((pos, kw)) = read_args(["prog", "aa", "--bb", "cc",
                                          "dd", "--ee"]
                                         .iter()
                                         .map(|s| s.to_string()),
                                         &["--ee"]) {
            assert_eq!(pos, expected_pos);
            assert_eq!(kw, expected_kw);
        } else {
            assert!(false)
        }
    }

    #[test]
    fn test_read_args_err() {
        if let Err(err) = read_args(["prog", "--aa"]
                                    .iter()
                                    .map(|s| s.to_string()),
                                    &[]) {
            assert!(err.ends_with("--aa"))
        } else {
            assert!(false)
        }
    }
}

