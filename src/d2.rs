// 2-D computations and drawings

use num_complex::Complex64;
use image;
use rayon::prelude::*;

#[path="config.rs"]
pub mod config;
pub use config::{Config, Method};


// Pixel iteration for Mandelbrot, Multibrot, and Julia
//
// Compute a pixel value for a complex number. This function is called by
// a higher level iterate function below.
fn iterate_multibrot_julia(pow: Complex64,
                           start: Complex64,
                           julia_add: Option<Complex64>) -> Option<u8> {
    let mut z = start;
    let addition = match julia_add {
        Some(num) => num,
        _ => start
    };
    let max = 64;
    for number in 1..max {
        // Do this because floats are not allowed in patterns
        if pow.im == 0.0 && pow.re == 2.0 {
            z = z * z + addition;
        } else if pow.im == 0.0 && pow.re == 3.0 {
            z = z * z * z + addition;
        } else {
            // This could be optimized further by using powc only when the
            // imaginary part is non-zero
            z = z.powc(pow) + addition;
        }
        if z.norm() > 4.0 {
            return Some(number * 4);
        }
    }
    None
}


// Dispatch the iteration to the right function
fn iterate_point(start: Complex64, method: &Method) -> Option<u8> {
    match method {
        Method::Mandelbrot =>
            iterate_multibrot_julia(Complex64::new(2.0, 0.0), start, None),
        Method::Multibrot(pow) =>
            iterate_multibrot_julia(*pow, start, None),
        Method::Julia(pow, addition) =>
            iterate_multibrot_julia(*pow, start, Some(*addition)),
	Method::Interactive => panic!(
	    "We shouldn't have entered here with interactive mode")
    }
}


// Compute the complex numbers for the pixels from the configuration
pub struct FractalBox<'a> {
    config: &'a Config,
    // Precompute these to avoid computation in the loop
    xstep: f64,
    ystep: f64,
}


impl FractalBox<'_> {

    pub fn new(config: &Config) -> FractalBox {
            let xstep = {
                let x0 = config.corner_ll.re;
                let x1 = config.corner_ur.re;
                let xlen = config.size.x as f64;
                (x1 - x0) / xlen
            };
            let ystep = {
                let y0 = config.corner_ll.im;
                let y1 = config.corner_ur.im;
                let ylen = config.size.y as f64;
                (y1 - y0) / ylen
            };
            FractalBox {
                config,
                xstep,
                ystep,
            }
        }

    fn comp_z(&self, xi: usize, yi: usize) -> Complex64 {
        let z0 = self.config.corner_ll;
        let dre = xi as f64 * self.xstep;
        let dim = yi as f64 * self.ystep;
        z0 + Complex64::new(dre, dim) 
    }

    fn start_vector(&self) -> Vec<Complex64> {
        let xlen = self.config.size.x;
        let ylen = self.config.size.y;
        let mut starts: Vec<Complex64> = Vec::with_capacity(xlen * ylen);
        for yi in 0..ylen {
            for xi in 0..xlen {
                starts.push(self.comp_z(xi, yi));
            }
        }
        starts
    }
}


pub fn make_pixels(config: &Config) -> Vec<u8> {
        FractalBox::new(config)
            .start_vector()
            .into_par_iter()
            .map(|z| iterate_point(z, &config.method).unwrap_or(0))
            .collect()
}


pub fn write_image(name: &str, pixels: &[u8], xlen: usize, ylen: usize)
    ->  Result<(), image::error::ImageError> {
        image::save_buffer(name, pixels, xlen as u32,
                           ylen as u32, image::ColorType::L8)
    }


#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_mandelbrot_origin() {
        let z = Complex64::new(0.0, 0.0);
        assert_eq!(iterate_point(z, &Method::Mandelbrot), None);
    }

    #[test]
    fn test_mandelbrot_point() {
        let z = Complex64::new(0.5, 0.0);
        assert_eq!(iterate_point(z, &Method::Mandelbrot), Some(20));
    }
}

