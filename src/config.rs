use std::str::FromStr;
use std::num::ParseIntError;

use num_complex::Complex64;

mod command_line;
use command_line::read_args;


const USAGE_CLI: &str = "[--help] [METHOD] [POWER] [POINT] [-o FILENAME] \
                         [--ll POINT] [--ur POINT] [-s SIZE]";

const HELP_CLI: &str = r#"

Visualize fractals in an interactive window or draw them to image
files.  These fractals are Mandelbrot like creatures based on the
weirdness of complex numbers.  An image displays a collection of
pixels where each pixels denotes a complex number. The horizontal is
the real axis and the vertical the imaginary axis of the plane of
complex numbers.

The interactive window only supports the Mandelbrot set for now.
Navigate with arrow keys, space and backspace.

OPTIONS
    METHOD        "Interactive" (default), "Mandelbrot", "Multibrot",
                      or "Julia".  If "Interactive", the other arguments
                      are ignored.
    POWER         Complex power for Multibrot and Julia
                      ("2.0" equals Mandelbrot)
    POINT         Complex number for Julia
    -o FILENAME   Image filename for the output, default "fractal.png"
    --ll POINT    Lower left pixel (complex number), default "-2.0-1.3i"
    --ur POINT    Upper right pixel (complex number), default "2.0+1.3i"
    -s SIZE       Image size (horizontal x vertical), default "1000x800"
"#;


#[derive(Debug, PartialEq)]
pub struct Config {
    pub method: Method,
    pub corner_ll: Complex64,
    pub corner_ur: Complex64,
    pub size: Size,
    pub filename: String,
}


#[derive(Debug, PartialEq)]
pub enum Method {
    Interactive,
    Mandelbrot,
    Multibrot(Complex64),
    Julia(Complex64, Complex64),
}


#[derive(Debug, PartialEq)]
pub struct Size {
    pub x: usize,
    pub y: usize,
}


impl FromStr for Size {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let nums: Vec<&str> = s.split('x').collect();

        let x = nums[0].parse()?;
        let y = nums[1].parse()?;
        Ok(Size { x, y })
    }
}


impl Config {
    // TODO: This does a lot of stuff that should be delegated in some way
    // to read_args
    pub fn from_command_line<T>(args: T) -> Result<Config, String>
    where
        T: Iterator<Item = String>,
    {
	// Make pos mutable to go to interactive mode if no positional args
        let (mut pos, kw) = read_args(args, &["--help", "-h"])?;

        if kw.contains_key("--help") | kw.contains_key("-h") {
            return Err(format!("Usage: {} {} {}", pos[0],
                               USAGE_CLI, HELP_CLI));
        }
        if pos.len() == 1 {
	    pos = vec![pos[0].clone(), "interactive".to_string()];
        }

        let filename = match kw.get("-o") {
            Some(filename) => filename.to_string(),
            _ => "fractal.png".to_string(),
        };
        let corner_ll = match kw.get("--ll") {
            Some(num) => match num.parse() {
                Ok(x) => x,
                _ => return Err(format!("--ll {} not understood", num)),
            },
            _ => Complex64::new(-2.0, -1.3),
        };
        let corner_ur = match kw.get("--ur") {
            Some(num) => match num.parse() {
                Ok(x) => x,
                _ => return Err(format!("--ur {} not understood", num)),
            },
            _ => Complex64::new(2.0, 1.3),
        };
        let size = match kw.get("-s") {
            Some(s) => match s.parse() {
                Ok(x) => x,
                _ => return Err(format!("--ur {} not understood", s)),
            },
            _ => Size { x: 1000, y: 800 },
        };

        let method = match &*pos[1] {
	    "interactive" => Method::Interactive,
	    "mandelbrot" => Method::Mandelbrot,
            "multibrot" => {
                if pos.len() < 3 {
                    return Err("No power for Multibrot".to_string());
                }
                match pos[2].parse() {
                    Ok(pow) => Method::Multibrot(pow),
                    _ => return Err(format!("Power {} not understood",
                                            pos[2])),
                }
            },
            "julia" => {
                if pos.len() < 4 {
                    return Err("Needs power for Multibrot and a complex point"
                               .to_string());
                }
                match (pos[2].parse(), pos[3].parse()) {
                    (Ok(pow), Ok(num)) => Method::Julia(pow, num),
                    (Ok(_), _) => return Err(format!(
                            "Complex number {} not understood", pos[1])),
                    (_, Ok(_)) => return Err(format!(
                            "Power {} not understood", pos[2])),
                    _ => return Err(format!(
                            "Number {} and power {} not understood",
                            pos[3], pos[2])),
                }
            },
            _ => return Err(format!("Method {} not supported", pos[1])),
        };
        Ok(Config {
            method,
            corner_ll,
            corner_ur,
            size,
            filename,
        })
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_cli_args() {
        let args = ["prog", "Julia", "2.5+i", "-0.1-0.3i", "--ll", "-1-i",
                    "--ur", "2+3i", "-s", "10x20", "-o", "test.png"]
                        .iter()
                        .map(|s| s.to_string());
        let config = Config::from_command_line(args).unwrap();
        let expected_config = Config {
            method: Method::Julia(Complex64::new(2.5, 1.0),
                                  Complex64::new(-0.1, -0.3)),
            corner_ll: Complex64::new(-1.0, -1.0),
            corner_ur: Complex64::new(2.0, 3.0),
            size: Size { x: 10, y: 20},
            filename: "test.png".to_string(),
        };
        assert_eq!(config, expected_config);
    }
}
