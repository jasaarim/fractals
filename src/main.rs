use std::env;
use std::process;

mod d2;
use d2::{make_pixels, write_image, Config, Method};

mod interactive;


fn main() {
    let config = Config::from_command_line(env::args()).unwrap_or_else(|err| {
        if err.starts_with("Usage") {
            println!("{}", err);
            process::exit(0)
        } else {
            eprintln!("{}", err);
            process::exit(1);
        }
    });
    if config.method == Method::Interactive {
        interactive::run();
    } else {
        let pixels = make_pixels(&config);
        let xlen = config.size.x;
        let ylen = config.size.y;
        write_image(&config.filename, &pixels, xlen, ylen).unwrap();
    }
}

