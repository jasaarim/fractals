// The contents of this file were initially copied from
// https://github.com/brendanzab/gl-rs/blob/master/gl/examples/triangle.rs
// after commit d142f6e6, and the copyright notice below belongs to that.

// Copyright 2015 Brendan Zabarauskas and the gl-rs developers
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate gl;
extern crate glutin;

use gl::types::*;
use std::ffi::CString;
use std::mem;
use std::ptr;
use std::str;
use std::collections::HashMap;


// Shader sources
static VS_SRC: &'static str = "
#version 430 core
layout (location = 0) in vec2 position;

void main() {
    gl_Position = vec4(position, 0.0, 1.0);
}";


static FS_SRC: &'static str = "
#version 430 core
out vec4 out_color;

uniform vec2 window_size;
uniform double dx, dy, zoom;

double scale = (1 - zoom);
dvec2 dp = dvec2(dx, dy);

void main() {

    dvec2 p = 2 * (gl_FragCoord.xy / window_size - vec2(0.5, 0.5));
    p *= scale;
    p += dp;
    dvec2 c = p;

    int max_iter;
    // Do this as log doesn't support double
    if (scale > 1e-2) {
         max_iter = 64;
    } else if (scale > 1e-4) {
         max_iter = 128;
    } else if (scale > 1e-6) {
         max_iter = 192;
    } else if (scale > 1e-8) {
         max_iter = 256;
    } else if (scale > 1e-10) {
         max_iter = 320;
    } else if (scale > 1e-12) {
         max_iter = 384;
    } else if (scale > 1e-14) {
         max_iter = 448;
    } else {
         max_iter = 512;
    }
    float color = 0.0;
    for (int i = 0; i < max_iter; i++) {
        p = dvec2(p.x * p.x - p.y * p.y, 2.0 * p.x * p.y) + c;
        if ((p.x * p.x + p.y * p.y) > 16.0) {
            color = float(i) / max_iter;
            break;
        }
    }
    out_color = vec4(pow(color, 2), pow(color, 0.5), color, 1.0);

}";


fn compile_shader(src: &str, ty: GLenum) -> GLuint {
    let shader;
    unsafe {
        shader = gl::CreateShader(ty);
        // Attempt to compile the shader
        let c_str = CString::new(src.as_bytes()).unwrap();
        gl::ShaderSource(shader, 1, &c_str.as_ptr(), ptr::null());
        gl::CompileShader(shader);

        // Get the compile status
        let mut status = gl::FALSE as GLint;
        gl::GetShaderiv(shader, gl::COMPILE_STATUS, &mut status);

        // Fail on error
        if status != (gl::TRUE as GLint) {
            let mut len = 0;
            gl::GetShaderiv(shader, gl::INFO_LOG_LENGTH, &mut len);
            let mut buf = Vec::with_capacity(len as usize);
            buf.set_len((len as usize) - 1); // subtract 1 to skip the trailing null character
            gl::GetShaderInfoLog(
                shader,
                len,
                ptr::null_mut(),
                buf.as_mut_ptr() as *mut GLchar,
            );
            panic!(
                "{}",
                str::from_utf8(&buf)
                    .ok()
                    .expect("ShaderInfoLog not valid utf8")
            );
        }
    }
    shader
}

fn link_program(vs: GLuint, fs: GLuint) -> GLuint {
    unsafe {
        let program = gl::CreateProgram();
        gl::AttachShader(program, vs);
        gl::AttachShader(program, fs);
        gl::LinkProgram(program);

        gl::DeleteShader(fs);
        gl::DeleteShader(vs);

        // Get the link status
        let mut status = gl::FALSE as GLint;
        gl::GetProgramiv(program, gl::LINK_STATUS, &mut status);

        // Fail on error
        if status != (gl::TRUE as GLint) {
            let mut len: GLint = 0;
            gl::GetProgramiv(program, gl::INFO_LOG_LENGTH, &mut len);
            let mut buf = Vec::with_capacity(len as usize);
            buf.set_len((len as usize) - 1); // subtract 1 to skip the trailing null character
            gl::GetProgramInfoLog(
                program,
                len,
                ptr::null_mut(),
                buf.as_mut_ptr() as *mut GLchar,
            );
            panic!(
                "{}",
                str::from_utf8(&buf)
                    .ok()
                    .expect("ProgramInfoLog not valid utf8")
            );
        }
        program
    }
}

fn create_window(window_size: (i32, i32))
		 -> (glutin::event_loop::EventLoop<()>,
		     glutin::WindowedContext<glutin::PossiblyCurrent>) {
    let event_loop = glutin::event_loop::EventLoop::new();
    let window = glutin::window::WindowBuilder::new()
	.with_title("Fractal")
        .with_inner_size(glutin::dpi::PhysicalSize::new(window_size.0,
						        window_size.1));
    let gl_window = glutin::ContextBuilder::new()
        .build_windowed(window, &event_loop)
        .unwrap();

    // It is essential to make the context current before calling `gl::load_with`.
    let gl_window = unsafe { gl_window.make_current() }.unwrap();

    // Load the OpenGL function pointers
    gl::load_with(|symbol| gl_window.get_proc_address(symbol));

    (event_loop, gl_window)
}

struct Program<'a> {
    id: GLuint,
    window: glutin::WindowedContext<glutin::PossiblyCurrent>,
    vao: u32,
    vbo: u32,
    ebo: u32,
    navi_controls: HashMap<&'a str, f64>,
    window_size: (i32, i32),
}


impl<'a> Program<'a> {

    fn new(vertex_shader: &str, fragment_shader: &str,
	   window: glutin::WindowedContext<glutin::PossiblyCurrent>,
           window_size: (i32, i32))
	   -> Program<'a> {
	let vs = compile_shader(vertex_shader, gl::VERTEX_SHADER);
	let fs = compile_shader(fragment_shader, gl::FRAGMENT_SHADER);
	let id = link_program(vs, fs);
	let vao = 0;
	let vbo = 0;
	let ebo = 0;

        let navi_controls: HashMap<&str, f64> =
	    [("dx", 0.0), ("dy", 0.0), ("zoom", 0.0)]
	    .iter().cloned().collect();

        Program { id, window, vao, vbo, ebo, navi_controls, window_size }
    }

    fn init(&mut self, vertex_data: &[GLfloat], vertex_indices: &[GLint]) {
	self.set_vertices(vertex_data, vertex_indices);
	unsafe {
	    gl::UseProgram(self.id);
	    let loc = gl::GetUniformLocation(self.id, CString::new("window_size").unwrap().as_ptr());
	    gl::Uniform2f(loc, self.window_size.0 as f32, self.window_size.1 as f32);
	}

	for (control, value) in &self.navi_controls {
            self.set_uniform(control, *value);
	}
    }

    fn set_vertices(&mut self, vertex_data: &[GLfloat], vertex_indices: &[GLint]) {
        unsafe {
            // Create Vertex Array Object
            gl::GenVertexArrays(1, &mut self.vao);
            gl::BindVertexArray(self.vao);

            // Create a Vertex Buffer Object and copy the vertex data to it
            gl::GenBuffers(1, &mut self.vbo);
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (vertex_data.len() * mem::size_of::<GLfloat>()) as GLsizeiptr,
                mem::transmute(&(*vertex_data)[0]),
                gl::STATIC_DRAW,
            );

	    gl::GenBuffers(1, &mut self.ebo);
	    gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.ebo);
    	    gl::BufferData(
                gl::ELEMENT_ARRAY_BUFFER,
    	        (vertex_indices.len() * mem::size_of::<GLint>()) as GLsizeiptr,
	        mem::transmute(&(*vertex_indices)[0]),
	        gl::STATIC_DRAW,
	    );

            // Specify the layout of the vertex data
            let pos_attr = gl::GetAttribLocation(self.id, CString::new("position").unwrap().as_ptr());
            gl::EnableVertexAttribArray(pos_attr as GLuint);
            gl::VertexAttribPointer(
                pos_attr as GLuint,
                2,
                gl::FLOAT,
                gl::FALSE as GLboolean,
                0,
                ptr::null(),
            );
	}
    }

    fn update_navi(&mut self, control: &'a str,  value: f64) {
	let new_value = match self.navi_controls.get(control) {
            Some(old_value) => old_value + value,
	    _ => value
	};
	self.navi_controls.insert(control, new_value);
        self.set_uniform(control, new_value);
	self.redraw();
    }

    fn xy_step(&mut self, x: bool, up_or_right: bool) {
	let control = if x {"dx"} else {"dy"};
	let sign = -1.0 + 2.0 * (up_or_right as i8 as f64);
	let scale = 1.0 - self.navi_controls.get("zoom").unwrap();
	let step = sign * scale * 0.15;
        self.update_navi(control, step);
    }

    fn z_step(&mut self, zoom_in: bool) {
        let scale = 1.0 - self.navi_controls.get("zoom").unwrap();
	let step = if zoom_in {scale / 5.0} else {-scale / 4.0};
        self.update_navi("zoom", step);
    }

    fn set_uniform(&self, name: &str, value: f64) {
        unsafe {
	    let loc = gl::GetUniformLocation(self.id,
				   CString::new(name).unwrap().as_ptr());
	    gl::Uniform1d(loc, value);
	};
    }

    fn cleanup(&self) {
        unsafe {
            gl::DeleteProgram(self.id);
            gl::DeleteBuffers(1, &self.vbo);
	    gl::DeleteBuffers(1, &self.ebo);
            gl::DeleteVertexArrays(1, &self.vao);
        }
    }

    fn redraw(&self) {
	unsafe {
	    gl::Viewport(0, 0, self.window_size.0, self.window_size.1);
            // Clear the screen to black
            gl::ClearColor(0.3, 0.3, 0.3, 1.0);
            gl::Clear(gl::COLOR_BUFFER_BIT);
            // Draw two triangles from the 6 vertices
            gl::DrawElements(gl::TRIANGLES, 6, gl::UNSIGNED_INT, ptr::null());
        }
	self.window.swap_buffers().unwrap();
    }
}


pub fn run() {
    let window_size = (900, 700);
    let(event_loop, window) = create_window(window_size);
    let mut program = Program::new(VS_SRC, FS_SRC, window, window_size);

    let vertex_data: [GLfloat; 8] = [1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0, 1.0];
    let vertex_indices: [GLint; 6] = [0, 1, 2, 0, 2, 3];

    program.init(&vertex_data, &vertex_indices);

    event_loop.run(move |event, _, control_flow| {
	use glutin::event::{Event, WindowEvent, VirtualKeyCode};
        use glutin::event_loop::ControlFlow;
        *control_flow = ControlFlow::Wait;
        match event {
            Event::LoopDestroyed => return,
            Event::WindowEvent { event, .. } => match event {
		WindowEvent::KeyboardInput { input, .. }  =>
                    if input.state == glutin::event::ElementState::Pressed {
		        match input.virtual_keycode {
                            Some(VirtualKeyCode::Q) => {
		                program.cleanup();
		                *control_flow = ControlFlow::Exit
		            },
		            Some(VirtualKeyCode::Left) => {
			        program.xy_step(true, false);
		            },
		            Some(VirtualKeyCode::Right) => {
			        program.xy_step(true, true);
		            },
			    Some(VirtualKeyCode::Up) => {
			        program.xy_step(false, true);
		            },
		            Some(VirtualKeyCode::Down) => {
			        program.xy_step(false, false);
		            },
			    Some(VirtualKeyCode::Space) => {
			        program.z_step(true);
		            },
			    Some(VirtualKeyCode::Back) => {
			        program.z_step(false);
		            },
		            _ => (),
			}
		    },
                WindowEvent::CloseRequested => {
		    program.cleanup();
                    *control_flow = ControlFlow::Exit
                },
                _ => (),
            },
            Event::RedrawRequested(_) => {
		program.redraw();
            },
            _ => (),
        }
    });
}

