# Fractals

This is my playground for learning Rust and it is work in progress. My
intention is to draw Mandelbrot-like fractal images.  At the time of
writing, the project provides a rudimentary interactive Mandelbrot set
explorer and a command-line interface for generating image files.

## Usage

You can get help on using the program with

```
$ cargo run -- --help
```

You can start the interactive Mandelbrot fractal explorer by

```
$ cargo run
```

The navigation happens with the arrow keys, space, and backspace.


To draw a Mandelbrot fractal in `fractal.png`, run

```
$ cargo run Mandelbrot
```

For Multibrot fractal with the power 3, run

```
$ cargo run Multibrot 3
```

To draw a Julia set for `f(z) = z^2 + 0.285 + 0.0i`, run

```
$ cargo run Julia 2 0.285+0.01i
```

